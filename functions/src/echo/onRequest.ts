import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
try {admin.initializeApp(functions.config().firebase);} catch(e) {}

/** HTTP Trigger */
exports = module.exports = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});