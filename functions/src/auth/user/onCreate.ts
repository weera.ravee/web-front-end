import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
try {admin.initializeApp(functions.config().firebase);} catch(e) {}

// Adds a message that welcomes new users into the system
exports = module.exports = functions.auth.user().onCreate((user) => {
    console.log('A new user signed in for the first time.');
    const fullName = user.displayName || 'Anonymous';

    // Saves the new welcome message into the database
    // which then displays it in the FriendlyChat clients.
    return admin.database().ref('messages').push({
        name: 'Firebase Bot',
        photoUrl: '/assets/images/firebase-logo.png', // Firebase logo
        text: `${fullName} signed in for the first time! Welcome!`
    });
});