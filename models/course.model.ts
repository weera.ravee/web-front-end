import { Base } from './base.model';

export class Course extends Base {
  title: string;
  description: string;
  startDate: string;
  endDate: string;
  archived: boolean;

  constructor(object: object = {}) {
    super(object);
  }
}
