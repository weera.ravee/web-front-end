import { Base } from './base.model';
import { User } from './user.model';
import { Course } from './course.model';

export class Enrollment extends Base {
  user: User;
  course: Course;

  constructor(object: object = {}) {
    super(object);
    if ('user' in object && object['user'] !== null) {
      Object.assign(this.user, new User(object['user']));
    }
  }
}
