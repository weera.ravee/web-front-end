import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SDragDropList, } from './drag-drop/s-drag-drop-list/s-drag-drop-list.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {A11yModule} from "@angular/cdk/a11y";
import {CdkStepperModule} from "@angular/cdk/stepper";
import {CdkTreeModule} from "@angular/cdk/tree";
import {CdkTableModule} from "@angular/cdk/table";
import { SGrid } from './grid/s-grid-component/s-grid.component';
import {MaterialModule} from "../../../../../apps/admin/src/app/imports/material.module";
import { SChip } from './chip/s-chip/s-chip.component';
import {SDragDropListItem} from "./drag-drop/s-drag-drop-list-item/s-drag-drop-list-item.component";
import {SChipList} from "./chip/s-chip-list/s-chip-list.component";
import { SGridTile } from './grid/s-grid-tile/s-grid-tile.component';
import {MatInputModule} from "@angular/material";
import {CarouselSliderComponent} from "./carousel-slider/carousel-slider.component";
import {CarouselModule} from "ngx-owl-carousel-o";

@NgModule({
  declarations: [SDragDropList, SDragDropListItem, SGrid, SChip, SChipList, SGridTile,CarouselSliderComponent],
  imports: [
    CommonModule,
    A11yModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MaterialModule,
    MatInputModule,
    CarouselModule
  ],
  exports: [SDragDropList,SDragDropListItem, SGrid, SChip, SChipList, SGridTile,CarouselSliderComponent]
})
export class CoreComponentsModule {
}
