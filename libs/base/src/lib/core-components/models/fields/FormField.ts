import {Validator} from "./Validator";
import {FireArrayField} from "./FireArrayField"
import {ChipList} from "@satipasala/base";

export interface FormField<T> {
  order: number;
  type: string;
  name: string;
  label: string;
  disabled:boolean;
  hint?: string;
  value?: T,
  validators?:FireArrayField<Validator>;
  chipList?:ChipList;
  icon?:string;
}
