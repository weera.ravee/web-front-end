import {FormField} from "./FormField";

export interface CheckBoxField extends FormField<boolean>{
  checked:boolean;
  indeterminate:boolean;
  labelPosition:string;
  disabled:boolean;
}
