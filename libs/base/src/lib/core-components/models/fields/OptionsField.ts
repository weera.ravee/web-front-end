import {FormField, Option} from "@satipasala/base";

export interface OptionsField<T> extends FormField<T>{
  options:Array<Option<T>>;
}
