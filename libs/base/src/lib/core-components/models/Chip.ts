import {ThemePalette} from "@angular/material";

export interface Chip {
  name: string;
  color: ThemePalette;
}
