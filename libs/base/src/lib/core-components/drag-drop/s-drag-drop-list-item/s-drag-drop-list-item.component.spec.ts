import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SDragDropListItem } from './s-drag-drop-list-item.component';

describe('DragDropListComponentComponent', () => {
  let component: SDragDropListItem;
  let fixture: ComponentFixture<SDragDropListItem>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SDragDropListItem ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SDragDropListItem);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
