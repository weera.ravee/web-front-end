import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerWrittenComponent } from './answer-written.component';

describe('AnswerWrittenComponent', () => {
  let component: AnswerWrittenComponent;
  let fixture: ComponentFixture<AnswerWrittenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerWrittenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerWrittenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
