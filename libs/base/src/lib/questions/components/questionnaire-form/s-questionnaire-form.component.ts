import {Component, Input, OnInit} from '@angular/core';
import {Questionnaire} from "../../model/Questionnaire";

@Component({
  selector: 's-questionnaire-form',
  templateUrl: './s-questionnaire-form.component.html',
  styleUrls: ['./s-questionnaire-form.component.scss']
})
export class SQuestionnaireForm implements OnInit {
  get questionnaire(): Questionnaire {
    return this._questionnaire;
  }
  @Input()
  set questionnaire(value: Questionnaire) {
    this._questionnaire = value;
    if(this._questionnaire != null){
      this._questionnaire.questions = this.getQuestions();
    }
  }

 private _questionnaire:Questionnaire;

  constructor() { }

  ngOnInit() {

  }
  getQuestions(){
    return this._questionnaire.questions.sort((a, b) => a.order - b.order);
  }

}
