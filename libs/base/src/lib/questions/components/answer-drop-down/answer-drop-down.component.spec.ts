import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerDropDownComponent } from './answer-drop-down.component';

describe('AnswerDropDownComponent', () => {
  let component: AnswerDropDownComponent;
  let fixture: ComponentFixture<AnswerDropDownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerDropDownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
