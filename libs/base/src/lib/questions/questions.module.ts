import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DynamicFormsModule} from "../dynamic-forms/dynamic-forms.module";
import { AnswerOptionsComponent } from './components/answer-options/answer-options.component';
import { AnswerWrittenComponent } from './components/answer-written/answer-written.component';
import { AnswerNumberComponent } from './components/answer-number/answer-number.component';
import { AnswerSelectComponent } from './components/answer-select/answer-select.component';
import { AnswerDropDownComponent } from './components/answer-drop-down/answer-drop-down.component';
import {SQuestionnaireForm} from "./components/questionnaire-form/s-questionnaire-form.component";
import {SQuestionComponent} from "./components/question/s-question.component";

@NgModule({
  declarations: [AnswerOptionsComponent, AnswerWrittenComponent, AnswerNumberComponent, AnswerSelectComponent, AnswerDropDownComponent,SQuestionnaireForm,SQuestionComponent],
  imports: [
    CommonModule,
    DynamicFormsModule
  ],
  exports:[SQuestionnaireForm]
})
export class QuestionsModule { }
