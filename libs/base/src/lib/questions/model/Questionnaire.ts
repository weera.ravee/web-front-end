
import {Question} from "./Question";

export interface Questionnaire {
  /*type:QuestionnaireType;*/
  name: string;
  id:string;
  questions: Question<any,any>[];
}


export enum QuestionnaireType {
  AGGRIGATED,
  INDIVIDUAL

}
