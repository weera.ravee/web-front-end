import {FormField} from "@satipasala/base";
import {Answer} from "./Answer";


export interface AnswerWritten extends FormField<String>, Answer<String>{
    name:string
    description:string
    type:string;

}
