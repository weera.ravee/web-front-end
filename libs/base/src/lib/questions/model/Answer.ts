import {FormField} from "@satipasala/base";

export interface Answer<VALUE_TYPE, FIELD extends FormField<any>> {
  id: string
  scoringMechanism: ScoringMechanism;
  value: VALUE_TYPE
  field: FIELD;
  name:string;
}


export interface ScoringMechanism {
  id: string;
  name: string;
  description: string;
  type: "SCALE" | "REVERSE_SCALE"
}
