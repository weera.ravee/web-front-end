
import {Label} from "@satipasala/model";
import {OptionsField} from "../../core-components/models/fields/OptionsField";
import {Answer} from "./Answer";
import {FormField} from "@satipasala/base";

export interface Question<VALUE_TYPE,FIELD extends FormField<any>> extends OptionsField<VALUE_TYPE>{
  id:string;
  description?:string
  questionType:QuestionType;
  labels:Label[];
  answer:Answer<VALUE_TYPE,FIELD>;
}


export enum QuestionType {

}

