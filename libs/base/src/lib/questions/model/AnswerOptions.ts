import {OptionsField} from "../../core-components/models/fields/OptionsField";
import {Answer} from "./Answer";


export interface AnswerOptions<T> extends  OptionsField<T>, Answer<T>{


}
