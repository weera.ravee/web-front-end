import {Option} from "@satipasala/base";
import {Answer, ScoringMechanism} from "../Answer";
import {OptionsField} from "../../../core-components/models/fields/OptionsField";

export class AnswerAlmostNever implements Answer<Number, OptionsField<Number>> {
  field: OptionsField<Number>;
  id: string;
  name: string;

  scoringMechanism: ScoringMechanism;
  value: Number;

  constructor() {
    this.name = "Answer Almost Never";
    this.field =<OptionsField<Number>> {
      options:[],
      label:"Answers",
      type:"radio",
      name:"answer",
      disabled:true
    }
    this.field.options.push(
      <Option<Number>> {
        imageUrl: "",
        label: "Almost Always",
        value: 1
      }, <Option<Number>> {
        imageUrl: "",
        label: "Very Frequently",
        value: 2
      }, <Option<Number>> {
        imageUrl: "",
        label: "Somewhat frequently",
        value: 3
      }, <Option<Number>> {
        imageUrl: "",
        label: "somewhat infrequently",
        value: 4
      }, <Option<Number>> {
        imageUrl: "",
        label: "Very Infrequently",
        value: 5
      }, <Option<Number>> {
        imageUrl: "",
        label: "Almost Never",
        value: 5
      });

  }


}
