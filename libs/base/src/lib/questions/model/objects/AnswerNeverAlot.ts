import {ChipList, Option} from "@satipasala/base";
import {Answer, ScoringMechanism} from "../Answer";
import {OptionsField} from "../../../core-components/models/fields/OptionsField";

export class AnswerNeverAlot implements Answer<Number, OptionsField<Number>> {
  field: OptionsField<Number>;
  id: string;
  scoringMechanism: ScoringMechanism;
  value: Number;
  name: string;
  constructor() {

    this.name = "Answer Never A lot";

    this.scoringMechanism = new class implements ScoringMechanism {
      description: "";
      label:"Answers";
      id: string;
      name: "sa";
      type: "REVERSE_SCALE";
    }

    this.field =<OptionsField<Number>> {
      options:[],
      type:"radio",
      name:"answer"
    }

    this.field.options.push(
      <Option<Number>> {
        imageUrl: "",
        label: "Never",
        value: 1
      }, <Option<Number>> {
        imageUrl: "",
        label: "A Little",
        value: 2
      }, <Option<Number>> {
        imageUrl: "",
        label: "Sometimes",
        value: 3
      }, <Option<Number>> {
        imageUrl: "",
        label: "A Lot",
        value: 4
      });

  }




}
