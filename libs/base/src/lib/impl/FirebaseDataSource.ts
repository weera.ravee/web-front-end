import {DataSource} from "@angular/cdk/table";
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {Observable, merge, Subject} from 'rxjs';
import {AngularFirestoreCollection, DocumentChangeAction, DocumentData, QueryFn} from "@angular/fire/firestore";
import {AfterViewInit, OnInit} from "@angular/core";
import {CollectionService, SubCollectionInfo} from "./CollectionService";
import {HostLocation} from "@satipasala/model";


/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class FirebaseDataSource<T> extends DataSource<T> implements AfterViewInit {
  data: AngularFirestoreCollection<T>;
  total: number = 100;//todo add total calculation to each collection in functions
  lastObj: T;
  dataSubject: Subject<T[]> = new Subject<T[]>();
  collectionService: CollectionService<any>;

  public constructor(protected matPaginator: MatPaginator, protected matSort: MatSort, protected colService: CollectionService<any>) {
    super();
    this.collectionService = colService;
  }


  /**
   * query data of a sub collection by giving documentId and sub collection name
   * @param pageIndex which index of page is retrieved
   * @param pageSize
   * @param subCollectionPaths [{documentId: 'course1', subCollection:'activities'}]
   */
  public queryData(queryFn: QueryFn, ...subCollectionPaths: SubCollectionInfo[]) {
    this.collectionService.querySubCollection(
      queryFn,
      ...subCollectionPaths
    ).subscribe(
      document => {
        this.dataSubject.next(document);
      });
  }

  /**
   * query data from multiple sub collections by giving documentId and sub collection name as rest parameters
   *
   * @param pageIndex which index of page is retrieved
   * @param pageSize
   * @param subCollectionPaths [{documentId: 'course1', subCollection:'activities'}]
   */
  public queryCombinedData(queryFn: QueryFn, subCollectionPaths: SubCollectionInfo[][]) {
    for (let i = 0; i < subCollectionPaths.length; i++) {
      this.queryData(queryFn, subCollectionPaths[i][0]);
    }
  }


  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<T[]> {
    return this.dataSubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.dataSubject.unsubscribe();
  }

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: AngularFirestoreCollection<T>) {/*
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);*/
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: AngularFirestoreCollection<T>) {
    /*if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });*/
  }

  ngAfterViewInit(): void {
    this.fetchData();
  }

  /**
   * load more data to collection using paginator
   * @param event
   */
  loadMore(event: PageEvent) {
    this.queryData(query => query.orderBy("id").startAt(event.pageIndex).limit(event.pageSize));
  }

  fetchData(): void {
    this.queryData(query => query.orderBy("id").startAt(0).limit(this.matPaginator.pageSize));
  }
}
