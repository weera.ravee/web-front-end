import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DynamicFormComponent} from "./components/dynamic-form-component/dynamic-form.component";
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule, MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule
} from "@angular/material";
import {CheckBoxComponent} from './components/fields/check-box/check-box.component';
import {DropDownComponent} from './components/fields/drop-down/drop-down.component';
import {RadioComponent} from './components/fields/radio/radio.component';
import {TextBoxComponent} from './components/fields/text-box/text-box.component';
import {FieldBuilderComponent} from './components/field-builder/field-builder.component';
import { InputComponent } from './components/fields/input/input.component';
import {ErrorStateMatcherFactory} from "./services/ErrorStateMatcherFactory";
import {SFieldError} from "./components/field-error/s-field-error.component";
import {
  FormFieldCustomControlExample,
  MyTelInput
} from "./components/fields/example-tel-input/example-tel-input.component";
import {MaterialModule} from "../imports/material.module";
import { SSelectionListComponent } from './components/fields/s-selection-list/s-selection-list.component';
import { DynamicStepperForm } from './components/dynamic-stepper-form-component/dynamic-stepper-form.component';

import {CoreComponentsModule} from "./../core-components/core-components.module";

@NgModule({
  declarations: [
    DynamicFormComponent,
    CheckBoxComponent, DropDownComponent, RadioComponent, TextBoxComponent, FieldBuilderComponent, InputComponent,
    SFieldError,MyTelInput,FormFieldCustomControlExample, SSelectionListComponent, DynamicStepperForm],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatCheckboxModule,
    MaterialModule,
    CoreComponentsModule
  ],

  providers:[ErrorStateMatcherFactory],

  exports: [DynamicFormComponent,DynamicStepperForm]
})
export class DynamicFormsModule {
}
