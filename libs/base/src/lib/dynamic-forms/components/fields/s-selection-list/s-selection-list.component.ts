import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ErrorStateMatcherFactory, FormField} from "@satipasala/base";
import {AbstractFieldComponent} from "../AbstractFieldComponent";
import {OptionsField} from "../../../../core-components/models/fields/OptionsField";

@Component({
  selector: 's-selection-list',
  templateUrl: './s-selection-list.component.html',
  styleUrls: ['./s-selection-list.component.scss']
})
export class SSelectionListComponent extends AbstractFieldComponent<OptionsField<any>>{

  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    super(errorStateMatcherFactory, cdRef)

  }
}
