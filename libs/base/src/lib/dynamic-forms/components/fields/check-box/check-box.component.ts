import {ChangeDetectorRef, Component, ElementRef, Input, OnInit, Optional, Self} from '@angular/core';
import {CheckBoxField} from "../../../../core-components/models/fields/CheckBoxField";
import {ErrorStateMatcherFactory} from "../../../services/ErrorStateMatcherFactory";
import {AbstractFieldComponent} from "../AbstractFieldComponent";

@Component({
  selector: 'satipasala-check-box',
  templateUrl: './check-box.component.html',
  styleUrls: ['./check-box.component.scss']
})
export class CheckBoxComponent  extends AbstractFieldComponent<CheckBoxField>{

  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
   super(errorStateMatcherFactory, cdRef)

  }
}

