import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Optional,
  Self,
} from '@angular/core';
import {ErrorStateMatcherFactory} from "../../../services/ErrorStateMatcherFactory";
import {AbstractFieldComponent} from "../AbstractFieldComponent";
import {OptionsField} from "../../../../core-components/models/fields/OptionsField";

@Component({
  selector: 'satipasala-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent  extends AbstractFieldComponent<OptionsField<any>>{
  selected: any;
  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    super(errorStateMatcherFactory, cdRef)
  }
}
