import {ChangeDetectorRef, Component} from '@angular/core';
import {AbstractFieldComponent} from "../AbstractFieldComponent";
import {ErrorStateMatcherFactory} from "@satipasala/base";
import {OptionsField} from "../../../../core-components/models/fields/OptionsField";

@Component({
  selector: 'satipasala-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss']
})
export class DropDownComponent extends AbstractFieldComponent<OptionsField<any>>{
  selected: any;
  constructor(public errorStateMatcherFactory: ErrorStateMatcherFactory,public cdRef: ChangeDetectorRef) {
    super(errorStateMatcherFactory, cdRef)

  }
}
