import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DynamicFormComponent, FormField} from "@satipasala/base";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";

@Component({
  selector: 's-dynamic-stepper-form',
  templateUrl: './dynamic-stepper-form.component.html',
  styleUrls: ['./dynamic-stepper-form.component.scss']
})
export class DynamicStepperForm extends DynamicFormComponent<FormField<any>> {

  isLinear = false;


  constructor() {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
}
