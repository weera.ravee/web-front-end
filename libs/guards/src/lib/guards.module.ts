import { NgModule } from '@angular/core';
import {AuthService, NotifyService, ServicesModule} from "@satipasala/services";


@NgModule({
  providers:[AuthService,NotifyService],
  imports:[
    ServicesModule
  ]
})
export class GuardsModule { }
