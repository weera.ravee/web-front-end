import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {CollectionService} from "@satipasala/base";
import {Activity} from "@satipasala/model";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class ActivitiesService extends CollectionService<Activity> {
  public static collection: string = "activities";

  constructor(protected fireStore: AngularFirestore) {
    super(ActivitiesService.collection, fireStore);
  }

  public getActivity(activityId, activityConsumer) {
    return this.fireStore.collection(this.collection).doc(activityId).valueChanges().subscribe(action => activityConsumer(action));
  }

}
