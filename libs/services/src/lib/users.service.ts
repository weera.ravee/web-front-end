import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {CollectionService} from "@satipasala/base";
import {User} from "@satipasala/model";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class UsersService extends CollectionService<User> {
  public static collection: string = "users";

  constructor(protected  fireStore: AngularFirestore) {
    super(UsersService.collection, fireStore);
  }

  /**
   *
   * @param uid ID of the user to find
   * @param userConsumer callback to the value change subscriber
   */
  public getUser(uid, userConsumer) {
    return this.fireStore.collection(this.collection).doc(uid).valueChanges().subscribe(action => userConsumer(action));
  }
}
