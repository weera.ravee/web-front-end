import { Injectable } from '@angular/core';
import {CollectionService} from "@satipasala/base";
import {Course} from "@satipasala/model";
import {AngularFirestore} from "@angular/fire/firestore";
import {ServicesModule} from "@satipasala/services";

@Injectable({
    providedIn: ServicesModule,
  }
)
export class CoursesService extends CollectionService<Course>{
  public static collection: string = "courses";

  constructor(protected fireStore: AngularFirestore) {
    super(CoursesService.collection, fireStore);
  }

  /**
   *
   * @param hostId ID of the host to find
   * @param hostConsumer callback to the value change subscriber
   */
  public getCourse(courseId, courseConsumer) {
    return this.fireStore.collection(this.collection).doc(courseId).valueChanges().subscribe(action => courseConsumer(action));
  }
}
