import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {CollectionService, Questionnaire} from "@satipasala/base";
import {ServicesModule} from "@satipasala/services";
@Injectable({
    providedIn: ServicesModule,
  }
)
export class QuestionnaireService extends CollectionService<Questionnaire> {

  public static collection: string = "questionnaires";

  constructor(protected fireStore: AngularFirestore) {
    super(QuestionnaireService.collection, fireStore);
  }
}
