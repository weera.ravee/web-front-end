import {Permission} from "./Permission";

export interface Role {
  id: string,
  name: string,
  allowedPermissions?: Permission[] | null,
  isActive?: boolean,
  createdAt?: Date | null,
  updatedAt?: Date | null,
}
