export interface Label {
  type: string;
  category: string;
  value: string;
}
