import {Activity} from "./Activity";
import {FeedbackForm} from "./FeedbackForm";
import {RefData} from "./RefData";

export class Program extends RefData {
  public id?: string;
  public name: string;
  public description?: string;
  public activities: Activity[];
  public facilitatorCount: number;
  public feedbackCollectionCount: number;
  public feedbackForms: FeedbackForm[];
}
