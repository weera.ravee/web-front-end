import {RefData} from "./RefData";

export class FeedbackForm extends RefData {
  public id?: string;
  public name: string;
  public description?: string;
}
