import {RefData} from "./RefData";

export class ActivityType extends RefData {
  public id?: string;
  public name: string;
  public description?: string;
}
