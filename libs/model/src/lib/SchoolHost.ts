import { Host } from './Host';
import { HostLocation } from './HostLocation';

export class SchoolHost implements Host {

    id: string;
    name: string;
    image: string;
    description: string;
    locations: HostLocation[];

    constructor() {
      this.id = "";
      this.name = "";
      this.image = "";
      this.description = "";
      this.locations = [];
    }
}
