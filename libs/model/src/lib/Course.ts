import {Activity} from "./Activity";

export interface Course {
  id :string
  code : string
  title : string
  description :string
  startDate :string
  endDate :string
  location :string
  parentCourseId :string
  courseType :string
  state : string
  createdAt :Date
  updatedAt :Date
  activities:Array<Activity>
}
