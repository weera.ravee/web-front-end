import { HostLocation } from './HostLocation';

export class ClassLocation implements HostLocation {

    id: string;    name: string;
    description: string;
    parentLocation: string;
    locationType: import("./LocationType").LocationType;
    createdAt: Date;
    updatedAt: Date;
    hostId: string


}