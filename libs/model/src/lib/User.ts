import {Role} from "@satipasala/model";
import {Permission} from "@satipasala/model";

export interface User {
  uid?: string,
  userName?: string | null,
  firstName?: string | null,
  lastName?: string | null,
  displayName?: string | null;
  userRole?: Role,
  userPermissions?: Array<Permission> | string | null,
  email?: string | null,
  photoURL?: string | null,
  password?: string | null,
  createdAt?: Date | null,
  updatedAt?: Date | null,
}
