import {HostLocation} from "./HostLocation"
export interface Host {
  id:string;
  name:string;
  image:string;
  description:string;
  locations:Array<HostLocation>;
  phone_number:string;
  business_reg_no:string;
  website:string;
  email:string;
  street:string;
  city:string;
  province:string;
  country:string;
  type:string;
  
}
