import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {CoursesService} from "@satipasala/services";
import {CoursesDataSource} from "./courses-data-source";

@Component({
  selector: 'admin-cources-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: CoursesDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'code', 'title','courseType', 'description', 'state', 'startDate', 'endDate', 'edit'];
  
  constructor(private  coursesService:CoursesService){

  }

  ngOnInit() {
    this.dataSource = new CoursesDataSource(this.paginator, this.sort, this.coursesService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }
}
