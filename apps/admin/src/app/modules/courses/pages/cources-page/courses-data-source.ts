import {FirebaseDataSource} from "@satipasala/base";
import {Course} from "@satipasala/model";
import {MatPaginator, MatSort} from "@angular/material";
import {CoursesService} from "@satipasala/services";

export class CoursesDataSource extends FirebaseDataSource<Course>{

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public coursesService: CoursesService) {
    super(paginator, sort, coursesService);
  }
}
