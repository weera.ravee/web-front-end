import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourceStatsComponent } from './cource-stats.component';

describe('CourceStatsComponent', () => {
  let component: CourceStatsComponent;
  let fixture: ComponentFixture<CourceStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourceStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourceStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
