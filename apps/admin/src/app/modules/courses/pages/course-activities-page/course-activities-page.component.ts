import {AfterViewInit, Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {COURSE_MANAGEMENT_ROUTE} from 'apps/admin/src/app/app-routs';


@Component({
  selector: 'admin-course-activity-page',
  templateUrl: './course-activities-page.component.html',
  styleUrls: ['./course-activities-page.component.css']
})

export class CourseActivitiesPage implements AfterViewInit {
  courseId: string;

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  gotoCoursePage() {
    this.router.navigate([COURSE_MANAGEMENT_ROUTE]);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.courseId) {
        this.courseId = params.courseId;
      } else {
        // TODO: Redirect to COURSE_MANAGEMENT_INFO_ROUTE
      }
    });

  }

  ngAfterViewInit(): void {
  }
}
