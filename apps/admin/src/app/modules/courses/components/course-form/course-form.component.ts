import {Component, Input, OnInit} from '@angular/core';
import {CoursesService} from "@satipasala/services";
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import {HostLocation, Host, Course} from '@satipasala/model';
import * as APP_ROUTES from "../../../../app-routs";

@Component({
  selector: 'admin-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.scss']
})
export class CourseFormComponent implements OnInit {

  courseForm: FormGroup;

  public course : Course;

  public courseId: string;

  public formTitle: string;

  public submitBtnText: string;

  constructor(private router: Router, private route : ActivatedRoute,private formBuilder : FormBuilder, private coursesService : CoursesService) { }

  ngOnInit() {
    this.courseForm = this.formBuilder.group({
      id : '',
      code : '',
      title : '',
      description : '',
      courseType : '',
      state : '',
      startDate : '',
      endDate : '',
    });

    /*This form receives edit, view, new*/
    this.route.params.subscribe(params => {
      this.courseId = params.courseId;
      if (params.courseId === 'new') {
        this.formTitle = "Create Course";
        this.submitBtnText = "Add";
        this.course = <Course>{};
        this.setCourse(this.courseForm)(this.course);
      } else{
        if (params.action === 'edit'){
          this.formTitle = "Edit Course";
          this.submitBtnText = "Update";
        }
        else{ // view
          this.formTitle = "View Course";
          this.submitBtnText = "";
        }
        this.coursesService.getCourse(params.courseId, this.setCourse(this.courseForm));
      }
    });
  }

  setCourse(form) {
    return course => {
      if(course){
        form.setValue({
          id : course.id,
          code : course.code,
          title : course.title,
          courseType : course.courseType,
          description : course.description,
          state: course.state,
          startDate : course.startDate,
          endDate : course.endDate,
        });
      } else {
        this.backToHostManage();
      }
    };
  }

  addEditCourse () {
    this.course = this.courseForm.value;
    if(this.courseId == "new") {
      this.coursesService.add(this.course);
    } else {
      this.coursesService.update(this.courseId, this.course);
    }
  }

  backToHostManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.COURSE_MANAGEMENT_ROUTE);
  }
}
