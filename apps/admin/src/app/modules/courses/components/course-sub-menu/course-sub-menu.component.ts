import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {COURSE_MANAGEMENT_ROUTE, HOST_MANAGEMENT_ROUTE} from "../../../../app-routs";
import {Course, Host} from "@satipasala/model";

@Component({
  selector: 'admin-course-sub-menu',
  templateUrl: './course-sub-menu.component.html',
  styleUrls: ['./course-sub-menu.component.css']
})
export class CourseSubMenuComponent implements OnInit {
  @Input() course:Course;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  editCourse() {
    this.router.navigateByUrl(COURSE_MANAGEMENT_ROUTE + "/" + this.course.id + "/edit");
  }

  viewCourse() {
    this.router.navigateByUrl(COURSE_MANAGEMENT_ROUTE + "/" + this.course.id + "/view");
  }

  showActivites(){
    this.router.navigateByUrl(COURSE_MANAGEMENT_ROUTE + "/" + this.course.id + "/activities");
  }
}
