import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {CourseActivityDataSource} from './course-activity-list-datasource';
import {ActivatedRoute} from "@angular/router";
import {ActivitiesService, CoursesService} from "@satipasala/services";

@Component({
  selector: 'admin-course-activity-list',
  templateUrl: './course-activity-list.component.html',
  styleUrls: ['./course-activity-list.component.scss']
})
export class CourseActivityListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: CourseActivityDataSource;
  @Input() courseId: string;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','description','edit'];
  constructor( private route:ActivatedRoute,private coursesService:CoursesService) {}

  ngOnInit() {
    this.dataSource = new CourseActivityDataSource(this.paginator, this.sort, this.coursesService, this.courseId);
  }

  ngAfterViewInit(): void {
    if (this.dataSource) {
      this.dataSource.ngAfterViewInit();
    }
  }

  loadMore(event: PageEvent) {
    if (this.dataSource) {
      this.dataSource.loadMore(event);
    }
  }
}
