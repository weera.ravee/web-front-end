import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {Activity, HostLocation} from "@satipasala/model";
import {AfterViewInit} from "@angular/core";
import {FirebaseDataSource} from "@satipasala/base";
import {CoursesService} from "@satipasala/services";


/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CourseActivityDataSource extends FirebaseDataSource<Activity> implements AfterViewInit {
  courseId: string;
  public static subCollection:string = "activities"

  constructor(public paginator: MatPaginator, private sort: MatSort,
              public coursesService: CoursesService, hostId: string) {
    super(paginator, sort, coursesService);
    this.courseId = hostId;
  }


  loadMore(event: PageEvent) {
    this.queryData(
      query => query.orderBy("id").startAt(event.pageIndex).limit(event.pageSize),
      {documentId: this.courseId, subCollection:CourseActivityDataSource.subCollection}
      );
  }

  ngAfterViewInit(): void {
    this.queryData(query => query.orderBy("id").startAt(0).limit(this.matPaginator.pageSize),
      {documentId: this.courseId, subCollection:CourseActivityDataSource.subCollection});
  }
}
