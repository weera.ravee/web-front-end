import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCreationPageComponent } from './question-creation-page.component';

describe('QuestionCreationPageComponent', () => {
  let component: QuestionCreationPageComponent;
  let fixture: ComponentFixture<QuestionCreationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCreationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCreationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
