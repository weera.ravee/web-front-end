import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Question} from "@satipasala/base";
import {Answer, ScoringMechanism} from "../../../../../../../../libs/base/src/lib/questions/model/Answer";
import {ReferenceDataService} from "@satipasala/services";
import {AnswerAlmostNever} from "../../../../../../../../libs/base/src/lib/questions/model/objects/AnswerAlmostNever";
import {AnswerNeverAlot} from "../../../../../../../../libs/base/src/lib/questions/model/objects/AnswerNeverAlot";

@Component({
  selector: 'admin-question-creation-page',
  templateUrl: './question-creation-page.component.html',
  styleUrls: ['./question-creation-page.component.scss']
})
export class QuestionCreationPageComponent implements OnInit {
  question: Question<any,any> = <Question<any,any>> {};
  answers:Answer<any,any>[] = [new AnswerAlmostNever(),new AnswerNeverAlot()];


  scoringMechanisms:ScoringMechanism[];

  selectedScoringMechanism:ScoringMechanism;
  selectedAnswer:Answer<any,any>;

  questionForm = this.fb.group({
    answer: [null, Validators.required],
    question:  [null, Validators.required],
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    address: [null, Validators.required],
    address2: null,
    city: [null, Validators.required],
    postalCode: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    shipping: ['free', Validators.required]
  });

  hasUnitNumber = false;


  constructor(private fb: FormBuilder, private referenceDataServie:ReferenceDataService) {
    this.question.labels = [
      {
        category: "question_group",
        type: "chip",
        value: "Beginner"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Advanced"
      },
      {
        category: "question_group",
        type: "chip",
        value: "Intermediate"
      }
    ];

    this.scoringMechanisms = this.referenceDataServie.getQuestionScoringMechanisms();
  }

  onSubmit(values) {
    alert(values);
  }

  ngOnInit(): void {
  }

}
