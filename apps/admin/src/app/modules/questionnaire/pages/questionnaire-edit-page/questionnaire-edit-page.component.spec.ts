import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireEditPage } from './questionnaire-page.component';

describe('QuestionnaireInfoPageComponent', () => {
  let component: QuestionnaireEditPage;
  let fixture: ComponentFixture<QuestionnaireEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireEditPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
