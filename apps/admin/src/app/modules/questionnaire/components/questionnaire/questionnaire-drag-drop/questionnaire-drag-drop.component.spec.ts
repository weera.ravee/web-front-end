import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireDragDropComponent } from './questionnaire-drag-drop.component';

describe('QuestionnaireDragDropComponent', () => {
  let component: QuestionnaireDragDropComponent;
  let fixture: ComponentFixture<QuestionnaireDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnaireDragDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
