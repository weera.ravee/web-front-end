import {FirebaseDataSource, Questionnaire} from "@satipasala/base";
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionnaireService} from "@satipasala/services";

export class QuestionnaireDataSource extends FirebaseDataSource<Questionnaire>{

  constructor(protected paginator: MatPaginator, protected sort: MatSort,
              protected questionnaireService: QuestionnaireService) {
    super(paginator, sort, questionnaireService);
  }

  /**
   * load more data to collection using paginator
   * @param event
   */
  loadMore(event: PageEvent) {
    this.queryData(query => query.orderBy("name").startAt(event.pageIndex).limit(event.pageSize));
  }

  fetchData(): void {
    this.queryData(query => query.orderBy("name").startAt(0).limit(this.matPaginator.pageSize));
  }
}
