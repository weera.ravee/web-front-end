import {Component, Input, OnInit} from '@angular/core';
import {QUESTION_MANAGEMENT_ROUTE} from "../../../../../app-routs";
import {Router} from "@angular/router";
import {Question} from "@satipasala/base";

@Component({
  selector: 'admin-question-sub-menu',
  templateUrl: './question-sub-menu.component.html',
  styleUrls: ['./question-sub-menu.component.scss']
})
export class QuestionSubMenuComponent implements OnInit {
  @Input() question: Question<any,any>;
  constructor(private router: Router) { }

  ngOnInit() {
  }


  editQuestion() {
    //this.router.navigate([QUESTIONNAIRE_MANAGEMENT_ROUTE + "/edit/" + this.questionnaire.id]);
    this.router.navigateByUrl(QUESTION_MANAGEMENT_ROUTE + "/edit/" + this.question.id);
  }

  viewQuestion() {
    this.router.navigate([QUESTION_MANAGEMENT_ROUTE + "/info/" + this.question.id]);
    /*,relativeTo: this.activatedRoute*/
  }

}
