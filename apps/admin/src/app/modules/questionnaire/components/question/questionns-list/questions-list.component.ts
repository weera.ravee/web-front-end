import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {QuestionnaireService, QuestionsService} from "@satipasala/services";
import {QuestionsListDataSource} from "./questions-list-data-source";
import {Router} from "@angular/router";
import {QUESTION_MANAGEMENT_CREATE_ROUTE, QUESTION_MANAGEMENT_ROUTE} from "../../../../../app-routs";

@Component({
  selector: 'admin-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss']
})
export class QuestionsListComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: QuestionsListDataSource;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','label', 'edit'];
  constructor(private  questionsService:QuestionsService, private router:Router){

  }

  ngOnInit() {
    this.dataSource = new QuestionsListDataSource(this.paginator, this.sort, this.questionsService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }

  addNewQuestion() {
    this.router.navigate([QUESTION_MANAGEMENT_CREATE_ROUTE]);
  }
}
