import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsDragDropComponent } from './questions-drag-drop.component';

describe('QuestionsDragDropComponent', () => {
  let component: QuestionsDragDropComponent;
  let fixture: ComponentFixture<QuestionsDragDropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsDragDropComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsDragDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
