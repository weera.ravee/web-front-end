import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Chip, ChipList, Question, Questionnaire} from "@satipasala/base";
import {QuestionType} from "../../../../../../../../../libs/base/src/lib/questions/model/QurstionType";
import {QuestionsService, ReferenceDataService} from "@satipasala/services";
import {QuestionsDragDropDataSource} from "./questions-drag-drop-data-source";
import {MatPaginator, PageEvent} from "@angular/material";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Observable} from "rxjs";

@Component({
  selector: 'admin-questions-drag-drop',
  templateUrl: './questions-drag-drop.component.html',
  styleUrls: ['./questions-drag-drop.component.scss']
})
export class QuestionsDragDropComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() questionnaire: Questionnaire;

  questionTypesChipList: ChipList = new class implements ChipList {
    stacked: false;
    type: "Question Types";
    values: QuestionType[];
  }

  questions: Question<any,any>[];

  dataSource: QuestionsDragDropDataSource;
  //todo change this numbers to get from collection.
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private referenceDataService: ReferenceDataService, private questionnaireService: QuestionsService) {

  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
  }

  ngOnInit() {
    this.questionTypesChipList.values = this.referenceDataService.getQuestionTypes();
    this.dataSource = new QuestionsDragDropDataSource(this.paginator, null, this.questionnaireService);
    this.dataSource.connect().subscribe(value => {
      this.questions = value;
    })
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect()
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  onQuestionTypeClick(chip: Chip) {
    console.log(chip);
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
}
