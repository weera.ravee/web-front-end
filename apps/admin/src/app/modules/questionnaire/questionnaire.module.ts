import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QuestionnaireRoutingModule} from './questionnaire-routing.module';
import {CoreComponentsModule, DynamicFormsModule, QuestionsModule} from "@satipasala/base";
import {QuestionnaireListComponent} from './components/questionnaire/questionnaire-list/questionnaire-list.component';
import {QuestionnaireListPage} from './pages/questionnaire-list-page/questionnaire-list-page.component';
import {QuestionnaireSubMenuComponent} from "./components/questionnaire/questionnaire-sub-menu/questionnaire-sub-menu.component";
import {MaterialModule} from "../../imports/material.module";
import {QuestionnaireService, ReferenceDataService, ServicesModule} from "@satipasala/services";
import { QuestionsPage } from './pages/questions-page/questions-page.component';
import { QuestionsListComponent } from './components/question/questionns-list/questions-list.component';
import { QuestionnaireInfoPage } from './pages/questionnaire-info-page/questionnaire-info-page.component';
import { QuestionnaireStepperComponent } from './components/questionnaire/questionnaire-stepper/questionnaire-stepper.component';
import { QuestionnaireDragDropComponent } from './components/questionnaire/questionnaire-drag-drop/questionnaire-drag-drop.component';
import { QuestionsDragDropComponent } from './components/question/questions-drag-drop/questions-drag-drop.component';
import {QuestionnaireEditPage} from "./pages/questionnaire-edit-page/questionnaire-edit-page.component";
import { QuestionCreationPageComponent } from './pages/question-creation-page/question-creation-page.component';
import { QuestionSubMenuComponent } from './components/question/question-sub-menu/question-sub-menu.component';
import { MatInputModule, MatButtonModule, MatSelectModule, MatRadioModule, MatCardModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [QuestionnaireListComponent, QuestionnaireSubMenuComponent, QuestionnaireListPage, QuestionnaireEditPage,
    QuestionsPage, QuestionsListComponent, QuestionnaireInfoPage, QuestionnaireStepperComponent, QuestionnaireDragDropComponent, QuestionsDragDropComponent, QuestionCreationPageComponent, QuestionSubMenuComponent],
  imports: [
    CommonModule,
    MaterialModule,
    QuestionnaireRoutingModule,
    DynamicFormsModule,
    CoreComponentsModule,
    QuestionsModule,
    ServicesModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule
  ]
})
export class QuestionnaireModule {
}
