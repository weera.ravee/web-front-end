import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from "@satipasala/services";
import {Role} from "@satipasala/model";

type UserFields = 'firstName' | 'lastName' | 'displayName' | 'userRole' | 'email' | 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'admin-user-registration-form',
  templateUrl: './user-registration-form.component.html',
  styleUrls: ['./user-registration-form.component.scss']
})
export class UserRegistrationFormComponent implements OnInit {
  userForm: FormGroup;
  formErrors: FormErrors = {
    'firstName': '',
    'lastName': '',
    'displayName': '',
    'userRole': '',
    'email': '',
    'password': '',
  };
  validationMessages = {
    'firstName': {
      'required': 'First Name is required.',
      'maxlength': 'First Name cannot be more than 40 characters long.',
    },
    'lastName': {
      'required': 'Last Name is required.',
      'maxlength': 'Last Name cannot be more than 40 characters long.',
    },
    'displayName': {
      'required': 'Display Name is required.',
      'maxlength': 'Display Name cannot be more than 40 characters long.',
    },
    'userRole': {
      'required': 'User Role is required.',
    },
    'email': {
      'required': 'Email is required.',
      'email': 'Email must be a valid email.',
    },
    'password': {
      'required': 'Password is required.',
      'pattern': 'Password must be include at one letter and one number.',
      'minlength': 'Password must be at least 4 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
  };

  // TODO: Remove hard-coded user roles and retrieve user roles from Firestore
  userRoles: Role[] = [
    {
      id: "SYSTEMADMIN",
      name: "System Administrator",
    },
    {
      id: "READONLY",
      name: "Read only access to the system",
    },
  ];

  constructor(private fb: FormBuilder, private auth: AuthService) {
  }

  ngOnInit() {
    this.buildForm();
  }

  onSubmit() {
    this.auth.createFirebaseUser(this.userForm.value['email'], this.userForm.value['password']);
    this.auth.registerUserByAdmin(
      this.userForm.value['firstName'],
      this.userForm.value['lastName'],
      this.userForm.value['displayName'],
      this.userForm.value['userRole'],
      this.userForm.value['email'],
      this.userForm.value['password'],
    ).then(() =>
      // TODO: Handle successful submits
      window.location.reload()
    );
  }

  buildForm() {
    this.userForm = this.fb.group({
      'firstName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'lastName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'displayName': ['', [
        Validators.required,
        Validators.maxLength(25),
      ]],
      'userRole': ['', [
        Validators.required,
      ]],
      'email': ['', [
        Validators.required,
        Validators.email,
      ]],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
      ]],
    });

    this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    if (!this.userForm) {
      return;
    }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field) && (field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
                this.formErrors[field] += `${(messages as { [key: string]: string })[key]} `;
              }
            }
          }
        }
      }
    }
  }
}
