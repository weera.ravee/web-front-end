import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserLoginFormComponent} from "./components/user-login-form/user-login-form.component";
import {UserRegistrationFormComponent} from "./components/user-registration-form/user-registration-form.component";
import {
  MatButtonModule,
  MatCardModule, MatIconModule,
  MatInputModule, MatMenuModule, MatPaginatorModule,
  MatRadioModule,
  MatSelectModule, MatSortModule,
  MatTableModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {UserProfileComponent} from "./components/user-profile/user-profile.component";
import {MaterialModule} from "../../imports/material.module";
import {NotificationMessageComponent} from "./components/notification-message/notification-message.component";
import {LoadingSpinnerComponent} from "./components/loading-spinner/loading-spinner.component";
import {AngularFirestore} from "@angular/fire/firestore";
import {AuthService, ServicesModule} from "@satipasala/services";
import {CoreComponentsModule} from "@satipasala/base";
import { AutoCompleteSearchComponent } from './components/auto-complete-search/auto-complete-search.component';

@NgModule({
  providers: [AngularFirestore,AuthService],

  declarations: [
    UserLoginFormComponent, UserRegistrationFormComponent, UserProfileComponent, NotificationMessageComponent, LoadingSpinnerComponent, AutoCompleteSearchComponent
  ],
  imports: [
    ServicesModule,
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    CoreComponentsModule,
    MaterialModule, //todo remove unwanted material modules here
  ],

  exports: [
    UserLoginFormComponent, UserRegistrationFormComponent, UserProfileComponent, NotificationMessageComponent, LoadingSpinnerComponent, AutoCompleteSearchComponent
  ]
})
export class CoreModule {
}
