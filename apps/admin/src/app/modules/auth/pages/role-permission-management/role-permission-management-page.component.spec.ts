import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePermissionManagementPage } from './role-permission-management-page.component';

describe('RolePermissionManagementPage', () => {
  let component: RolePermissionManagementPage;
  let fixture: ComponentFixture<RolePermissionManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolePermissionManagementPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePermissionManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
