import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RoleSubMenuComponent} from "./role-sub-menu.component";
import {RouterTestingModule} from "@angular/router/testing";
import {MaterialModule} from "../../../../imports/material.module";

import { RoleSubMenuComponent } from './role-sub-menu.component';

describe('RoleSubMenuComponent', () => {
  let component: RoleSubMenuComponent;
  let fixture: ComponentFixture<RoleSubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleSubMenuComponent ],
      imports: [
        MaterialModule,
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
