import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Host, Role} from "@satipasala/model";
import {AUTH_MANAGEMENT_ROUTE, AUTH_MANAGEMENT_ROUTE_ROLES} from "../../../../app-routs";

@Component({
  selector: 'admin-role-sub-menu',
  templateUrl: './role-sub-menu.component.html',
  styleUrls: ['./role-sub-menu.component.scss']
})
export class RoleSubMenuComponent implements OnInit {

  @Input() role:Role;
  constructor(private router: Router, private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
  }

  edit() {
    this.router.navigateByUrl(AUTH_MANAGEMENT_ROUTE_ROLES + "/" + this.role.id);
  }

  showPermissions(){
    this.router.navigate([this.role.id + "/permissions"],{relativeTo:this.activatedRoute});/*,relativeTo: this.activatedRoute*/
  }

}
