import {Component, OnInit} from '@angular/core';
import {Permission, Role} from "@satipasala/model";
import {ActivatedRoute, Router} from "@angular/router";
import {PermissionsService, RolesService} from "@satipasala/services";
import {Observable} from "rxjs";

@Component({
  selector: 'admin-role-permission-info',
  templateUrl: './role-permission-info.component.html',
  styleUrls: ['./role-permission-info.component.scss']
})
export class RolePermissionInfoComponent implements OnInit {

  allowedPermissions: Permission[];
  permissions: Observable<Permission[]>;

  constructor(protected router: Router, protected route: ActivatedRoute,
                        protected rolesService: RolesService, protected permissionsService: PermissionsService) {
  }

  ngOnInit() {
    this.permissions = this.permissionsService.getAll();
    this.route.params.subscribe(params => {
      if (params.roleId) {
        let roleId = params.roleId;
        this.rolesService.get(roleId).subscribe(roleDoc => {
          this.allowedPermissions = roleDoc.allowedPermissions;
        })
      } else {
        // TODO:
      }
    });
  }

  public isPermissionEnabled(permissionToCheck:string): boolean{
    console.log("Called : "+permissionToCheck);
    this.allowedPermissions.forEach(permission=> {
      if(permission.name.trim() ===  permissionToCheck){
        console.log("having permission "+permission.name);
        return true;
      } else {
        return false;
      }
    })
    console.log("No permission "+permissionToCheck);
    return false;
  }
}
