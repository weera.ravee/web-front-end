import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from "@angular/material";
import {RolesService} from "@satipasala/services";
import {FirebaseDataSource} from "@satipasala/base";
import {Role} from "@satipasala/model";

@Component({
  selector: 'admin-role-info',
  templateUrl: './role-info.component.html',
  styleUrls: ['./role-info.component.scss']
})
export class RoleInfoComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: FirebaseDataSource<Role>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'edit'];
  pageEvent: PageEvent;

  constructor(private rolesService: RolesService) { }

  ngOnInit() {
    this.dataSource = new FirebaseDataSource<Role>(this.paginator, this.sort, this.rolesService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }
}
