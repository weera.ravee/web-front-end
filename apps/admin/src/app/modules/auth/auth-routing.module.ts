import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AUTH_MANAGEMENT_ROUTE_PERMISSIONS, AUTH_MANAGEMENT_ROUTE_ROLES, AUTH_MANAGEMENT_ROUTE_ROLES_FORM,AUTH_MANAGEMENT_ROUTE_ROLES_PERMISSION

} from "../../app-routs";
import {RoleFormComponent} from "./components/role-form/role-form.component";
import {RoleManagementPage} from "./pages/role-management/role-management-page.component";
import {PermissionManagementPage} from "./pages/permission-management/permission-management-page.component";
import {RolePermissionManagementPage} from "./pages/role-permission-management/role-permission-management-page.component";

const routes: Routes = [
  {path: AUTH_MANAGEMENT_ROUTE_PERMISSIONS, component: PermissionManagementPage},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES, component: RoleManagementPage},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES_FORM, component: RoleFormComponent},
  {path: AUTH_MANAGEMENT_ROUTE_ROLES_PERMISSION, component: RolePermissionManagementPage}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
