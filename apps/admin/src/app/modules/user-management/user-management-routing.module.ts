import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {
  USERS_EDIT_ROUTE,
  USERS_MANAGEMENT_ROUTE,
  USERS_REGISTER_ROUTE,
} from "../../app-routs";
import {UserManagementPageComponent} from "./pages/user-management-page/user-management-page.component";
import {UserRegistrationPageComponent} from "./pages/user-registration-page/user-registration-page.component";
import {UserEditPageComponent} from "./pages/user-edit-page/user-edit-page.component";
import {TextSearchComponent} from "./components/text-search/text-search.component";

const routes: Routes = [
  {path: '', redirectTo: USERS_MANAGEMENT_ROUTE, pathMatch: "full"},
  {path: USERS_MANAGEMENT_ROUTE, component: UserManagementPageComponent},
  {path: USERS_REGISTER_ROUTE, component: UserRegistrationPageComponent},
  {path: 'search', component: TextSearchComponent},
  {path: USERS_EDIT_ROUTE, component: UserEditPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManagementRoutingModule {
}
