import {Component, OnInit} from '@angular/core';
import {UsersService} from "@satipasala/services";
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {User} from '@satipasala/model';
import * as APP_ROUTES from "../../../../app-routs";

@Component({
  selector: 'admin-user-edit-form',
  templateUrl: './user-edit-form.component.html',
  styleUrls: ['./user-edit-form.component.scss']
})
export class UserEditFormComponent implements OnInit {

  userEditForm: FormGroup;

  public user: User;
  public uid: string;
  public formTitle: string;
  public submitBtnText: string;

  constructor(private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, private UsersService: UsersService) {
  }

  ngOnInit(): void {
    this.userEditForm = this.formBuilder.group({
      uid: '',
      firstName: '',
      lastName: '',
      displayName: '',
      email: '',
    });
    this.route.params.subscribe(params => {
      this.uid = params.uid;
      this.formTitle = "Edit User";
      this.submitBtnText = "Update";
      this.UsersService.getUser(params.uid, this.setUser(this.userEditForm));
    });
  }

  setUser(form) {
    return user => {
      console.log(user);
      if (user) {
        form.setValue({
          uid: user.uid,
          firstName: user.firstName,
          lastName: user.lastName,
          displayName: user.displayName,
          email: user.email,
        });
      } else {
        this.backToUserManage();
      }
    };
  }

  editUser() {
    this.user = this.userEditForm.value;
    this.UsersService.update(this.uid, this.user);
  }

  backToUserManage() {
    console.log(APP_ROUTES.USERS_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.USERS_ROUTE);
  }
}
