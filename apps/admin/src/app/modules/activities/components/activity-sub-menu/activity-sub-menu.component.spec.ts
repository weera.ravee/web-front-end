import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitySubMenuComponent } from './activity-sub-menu.component';

describe('ActivitySubMenuComponent', () => {
  let component: ActivitySubMenuComponent;
  let fixture: ComponentFixture<ActivitySubMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitySubMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitySubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
