import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActivitiesRoutingModule} from './activities-routing.module';
import {DynamicFormsModule} from "@satipasala/base";
import {ActivitiesService, ServicesModule} from "@satipasala/services";
import {CoursesRoutingModule} from "../courses/courses-routing.module";
import {
  MatButtonModule,
  MatCardModule, MatIconModule,
  MatInputModule, MatMenuModule, MatPaginatorModule,
  MatRadioModule,
  MatSelectModule, MatSortModule,
  MatTableModule
} from "@angular/material";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ActivitiesPage } from "./pages/activities-page/activities-page.component";
import { ActivityListComponent } from "./components/activity-list/activity-list.component";
import { ActivityFormComponent } from './components/activity-form/activity-form.component';
import { ActivitySubMenuComponent } from './components/activity-sub-menu/activity-sub-menu.component';


@NgModule({
  declarations: [ActivitiesPage, ActivityListComponent, ActivityFormComponent, ActivitySubMenuComponent],
  imports: [
    CommonModule,
    ActivitiesRoutingModule,
    DynamicFormsModule,
    ServicesModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ActivitiesService]
})
export class ActivitiesModule {
}
