import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivitiesPage } from "./pages/activities-page/activities-page.component";
import { ActivityFormComponent } from './components/activity-form/activity-form.component';
import { ACTIVITY_MANAGEMENT_ACTION_ROUTE, ACTIVITY_MANAGEMENT_EDIT_ROUTE } from '../../app-routs';

const routes: Routes = [
  {path: '', component: ActivitiesPage},
  {path : ACTIVITY_MANAGEMENT_EDIT_ROUTE, component : ActivityFormComponent},
  {path : ACTIVITY_MANAGEMENT_ACTION_ROUTE, component : ActivityFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivitiesRoutingModule { }
