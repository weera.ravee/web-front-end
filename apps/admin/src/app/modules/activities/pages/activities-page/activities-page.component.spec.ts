import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivitiesPage } from './activities-page.component';
import { MaterialModule } from 'apps/admin/src/app/imports/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import {ActivitiesService} from "@satipasala/services";
import {ActivityListComponent} from "../../components/activity-list/activity-list.component";
import {ActivatedRoute, Params} from "@angular/router";
import {AngularFirestore} from "@angular/fire/firestore";
import {MockFireStore} from "@satipasala/testing";
import {of} from "rxjs";
import {ActivitySubMenuComponent} from "../../components/activity-sub-menu/activity-sub-menu.component";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('ActivitiesPage', () => {
  let component: ActivitiesPage;
  let fixture: ComponentFixture<ActivitiesPage>;
  let params:Params = {id: 1}
  const fakeActivatedRoute = {
    params: of(params)
  } as ActivatedRoute;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesPage ,ActivityListComponent, ActivitySubMenuComponent],

      imports: [
        MaterialModule,
        RouterTestingModule,
        NoopAnimationsModule
      ],
      providers:[ActivitiesService,{provide: ActivatedRoute, useValue: fakeActivatedRoute},{provide: AngularFirestore, useValue: MockFireStore}],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
