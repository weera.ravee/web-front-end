import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HostManagementRoutingModule } from './host-management-routing.module';
import { HostManagementPageComponent } from './pages/host-management-page/host-management-page.component';
import { HostInfoComponent } from './components/host-info-component/host-info.component';
import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatButtonModule,
  MatMenuModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatCardModule,
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import {HostSubMenuComponent} from "./components/host-sub-menu/host-sub-menu.component";
import {AngularFirestore} from "@angular/fire/firestore";
import {LocationSubMenuComponent} from "./components/location-sub-menu/location-sub-menu.component";
import {LocationInfoComponent} from "./components/location-info-component/location-info.component";
import {HostsService} from "@satipasala/services";
import {ServicesModule} from "@satipasala/services";
import { LocationFormComponent } from './components/location-form-component/location-form.component';
import { HostFormComponent } from './components/host-form/host-form.component';
import { DynamicFormsModule, CoreComponentsModule } from '@satipasala/base';

@NgModule({
  declarations: [HostManagementPageComponent, HostInfoComponent,HostSubMenuComponent,LocationInfoComponent,LocationSubMenuComponent, LocationFormComponent, HostFormComponent ],
  imports: [
    CommonModule,
    CoreComponentsModule,
    DynamicFormsModule,
    HostManagementRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ServicesModule
  ],

  providers: [HostsService]
})
export class HostManagementModule { }
