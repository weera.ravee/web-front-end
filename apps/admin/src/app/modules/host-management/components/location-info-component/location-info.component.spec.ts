import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationInfoComponent } from './location-info.component';
import {RouterTestingModule} from "@angular/router/testing";
import {MaterialModule} from "../../../../imports/material.module";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {ActivatedRoute, Params} from "@angular/router";
import {LocationSubMenuComponent} from "../location-sub-menu/location-sub-menu.component";
import {AngularFirestore} from "@angular/fire/firestore";
import {HostsService} from "@satipasala/services";
import {MockFireStore} from "@satipasala/testing";
import {of} from "rxjs";

describe('LocationInfoComponent', () => {

  let component: LocationInfoComponent;
  let fixture: ComponentFixture<LocationInfoComponent>;
  let params:Params = {id: 1}
    const fakeActivatedRoute = {
      params: of(params)
    } as ActivatedRoute;



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[HostsService,{provide: ActivatedRoute, useValue: fakeActivatedRoute},{provide: AngularFirestore, useValue: MockFireStore}],
      declarations: [ LocationInfoComponent ,LocationSubMenuComponent],
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        RouterTestingModule.withRoutes([]),

      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
