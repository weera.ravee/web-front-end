import {AfterViewInit, Component, OnInit, ViewChild, Input} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {LocationInfoDataSource} from './location-info-data-source';
import {ActivatedRoute} from "@angular/router";
import {HostsService} from "@satipasala/services";
import { FirebaseDataSource } from '@satipasala/base';
import { HostLocation } from '@satipasala/model';

@Component({
  selector: 'admin-location-info-component',
  templateUrl: './location-info.component.html',
  styleUrls: ['./location-info.component.scss']
})
export class LocationInfoComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: FirebaseDataSource<HostLocation>;
  hostId: string;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name','description','edit'];
  constructor( private route:ActivatedRoute,private hostsService:HostsService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
        if (params.hostId) {
          this.hostId = params.hostId;
          this.dataSource = new LocationInfoDataSource(this.paginator, this.sort, this.hostsService, this.hostId);
        } else {
          // TODO: Redirect to HOST_MANAGEMENT_INFO_ROUTE
        }
    });
  }

  ngAfterViewInit(): void {
    if(this.dataSource){
      this.dataSource.ngAfterViewInit();
    }
  }

  loadMore(event: PageEvent) {
    if(this.dataSource) {
      this.dataSource.loadMore(event);
    }
  }
}
