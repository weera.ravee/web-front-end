import { Component, OnInit } from '@angular/core';

import {HostsService} from "@satipasala/services";
import {ReferenceDataService} from "@satipasala/services";
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { HostLocation, Host } from '@satipasala/model';
import * as APP_ROUTES from "../../../../app-routs";

@Component({
  selector: 'admin-host-form',
  templateUrl: './host-form.component.html',
  styleUrls: ['./host-form.component.scss']
})
export class HostFormComponent implements OnInit {

  hostForm: FormGroup;

  public host : Host;
  public hostLocation : HostLocation;

  public hostId: string;

  public formTitle: string;
  public hostTypeChangeHidden: boolean;
  public submitBtnText: string;
  public types: any;
  
  
  constructor(private router: Router, private route : ActivatedRoute,private formBuilder : FormBuilder, private HostsService : HostsService, private ReferenceDataService : ReferenceDataService) { }

  ngOnInit() {
  
    this.ReferenceDataService.getHostTypes(this.setHostType());
    
    this.hostForm = this.formBuilder.group({
                              id : '',
                              name : '',
                              image : '',
                              description : '',
                              locations: [],
                              phone_number:'',
                              business_reg_no:'',
                              website:'',
                              email:'',
                              street:'',
                              city:'',
                              province:'',
                              country:'',
                              type:'',
                              
                              
                            });
    
    this.route.params.subscribe(params => {
      this.hostId = params.hostId;
      if (params.hostId === 'new') {
        this.formTitle = "Create Host";
        this.submitBtnText = "Add";
        this.hostTypeChangeHidden = false;
        this.host = <Host>{};
       
      } else {
        this.formTitle = "Edit Host";
        this.submitBtnText = "Update";
        this.hostTypeChangeHidden = true;
        this.HostsService.getHost(params.hostId, this.setHost(this.hostForm));
      }
    });
  }

  setHost(form : FormGroup) {
    return host => {
      if(host){
        form.patchValue({
          id : host.id,
          name : host.name,
          image : host.image,
          description : host.description,
          locations: host.locations,
          phone_number: host.phone_number,
          business_reg_no: host.business_reg_no,
          website: host.website,
          email: host.email,
          street: host.street,
          city: host.city,
          province: host.province,
          country: host.country,
          type: host.type          
        });
       } else {
        this.backToHostManage();
       }
     };
  }

  setHostType(){
    return hostType => {
      this.types = hostType.types;      
      
    };
  }

  addEditHost () {
    this.host = this.hostForm.value;
    
    if(this.hostId == "new") {
     
      this.HostsService.add(this.host).then(()=>{
        this.backToHostManage()
      }).catch(err =>{
        console.error(err)
      })
    
     
          
    } else {
      this.HostsService.update(this.hostId, this.host);
    }
  }

  backToHostManage() {
    console.log(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
    this.router.navigateByUrl(APP_ROUTES.HOST_MANAGEMENT_ROUTE);
  }

}
