import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, PageEvent} from '@angular/material';
import {HostsService} from "@satipasala/services";
import {FirebaseDataSource} from "@satipasala/base";
import {Host} from "@satipasala/model";

@Component({
  selector: 'admin-host-info-component',
  templateUrl: './host-info.component.html',
  styleUrls: ['./host-info.component.scss']
})
export class HostInfoComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: FirebaseDataSource<Host>;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['name', 'description', 'edit'];
  pageEvent: PageEvent;

  constructor(private hostsService: HostsService) {

  }

  ngOnInit() {
    this.dataSource = new FirebaseDataSource<Host>(this.paginator, this.sort, this.hostsService);
  }

  ngAfterViewInit(): void {
    this.dataSource.ngAfterViewInit();
  }

  loadMore(event: PageEvent) {
    this.dataSource.loadMore(event);
  }
}
