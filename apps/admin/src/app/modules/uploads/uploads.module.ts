import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UploadsRoutingModule} from "./uploads-routing.module";
import {UploadPageComponent} from './upload-page/upload-page.component';
import {DropZoneDirective} from './drop-zone.directive';
import {FileSizePipe} from './file-size.pipe';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {FileUploadModule} from 'ng2-file-upload';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatCardModule, MatIconModule, MatProgressBarModule} from "@angular/material";
import {HttpClientModule} from "@angular/common/http";
import {CustomUploadComponent} from './custom-upload/custom-upload.component';
import {UploadTaskComponent} from './upload-task/upload-task.component';

@NgModule({
  declarations: [
    UploadPageComponent,
    FileUploadComponent,
    DropZoneDirective,
    FileSizePipe,
    CustomUploadComponent,
    UploadTaskComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatProgressBarModule,
    UploadsRoutingModule,
    HttpClientModule,
    FileUploadModule
  ],
  exports: [
    FileUploadComponent
  ]
})
export class UploadsModule { }
