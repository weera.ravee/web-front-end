import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventBannerComponent } from './components/event-banner/event-banner.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, EventBannerComponent],
  declarations: [EventBannerComponent],
  entryComponents: [EventBannerComponent]
})
export class EventRoutingModule { }
