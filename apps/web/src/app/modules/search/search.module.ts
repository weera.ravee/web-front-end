import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';


@NgModule({
  declarations: [ ],
  imports: [
    CommonModule,
    SearchRoutingModule
  ],
  exports: [ ]
})
export class SearchModule { }

